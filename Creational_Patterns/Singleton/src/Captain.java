/**
 * Created by ramon on 3/04/17.
 */
public class Captain {

    private static Captain captain;

    // Makes the constructor private to prevent the use of "new"
    private Captain() {

    }

    // Method to create a captain
    public static Captain getCaptain() {

        if (captain == null) {
            captain = new Captain();
            System.out.println("New captain selected for your team");
        } else {
            System.out.println("You already have a captain in your team");
        }

        return captain;
    }
}
