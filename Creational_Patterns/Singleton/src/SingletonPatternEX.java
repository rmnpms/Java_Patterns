/**
 * Created by ramon on 3/04/17.
 */
public class SingletonPatternEX {

    public static void main(String[] args) {

        System.out.println("Singleton Pattern Demo \n");
        System.out.println("Making a captain for your team:");
        Captain captain1 = Captain.getCaptain();
        System.out.println();
        System.out.println("Trying to make another captain:");
        Captain captain2 = Captain.getCaptain();
    }
}
