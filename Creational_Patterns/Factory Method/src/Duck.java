/**
 * Created by ramon on 19/04/17.
 */
public class Duck implements IAnimal{

    //Constructor
    public Duck() {

    }

    //Methods
    public void Speak() {
        System.out.println("Duck says cuack-cuack");
    }
}
