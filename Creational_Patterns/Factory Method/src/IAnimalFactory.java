/**
 * Created by ramon on 19/04/17.
 */
abstract class IAnimalFactory {

    public abstract IAnimal GetAnimalType(String type) throws Exception;
}

