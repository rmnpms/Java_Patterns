import java.util.Random;

/**
 * Created by ramon on 3/04/17.
 */
public class BasicCar implements Cloneable {

    public String modelName;
    public int price;

    // Constructor
    public BasicCar() {

    }

    // Getters and Setters
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }


    public static int setPrice() {
        int price = 0;
        Random r = new Random();
        int p = r.nextInt(100000);
        price = p;
        return price;

    }

    // Methods
    public BasicCar clone() throws CloneNotSupportedException {
        return (BasicCar)super.clone();

    }
}
