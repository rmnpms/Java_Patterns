/**
 * Created by ramon on 3/04/17.
 */
public class Nano extends BasicCar {

    // Constructor
    public Nano(String m) {
        modelName = m;
    }

    // Methods
     @Override
    public BasicCar clone() throws CloneNotSupportedException {
         return (Nano) super.clone();
     }
}
