/**
 * Created by ramon on 3/04/17.
 */
public class Ford extends BasicCar {

    public Ford(String m) {
        modelName = m;
    }

    // Methods
    @Override
    public BasicCar clone() throws CloneNotSupportedException {
        return (Ford)super.clone();
    }
}
