/**
 * Created by ramon on 3/04/17.
 */
public class PrototypePatternExample {

    public static void main(String args[]) throws CloneNotSupportedException {
        System.out.println("Prototype Pattern Demo \n");
        System.out.println();
        BasicCar nano_base = new Nano("Blue Nano");
        nano_base.price = 100000;
        System.out.println("Car is: " + nano_base.modelName + " and it's price is Rs." + nano_base.price);
        System.out.println();

        BasicCar ford_basic = new Ford("Red Ford");
//        ford_basic.price = 500000;
        ford_basic.price = BasicCar.setPrice();
        System.out.println("Car is: " + ford_basic.modelName + " and it's price is Rs." + ford_basic.price);
        System.out.println();

        // Creates new Nano car from the existing one
        BasicCar clonedBasiCarNum1 = nano_base.clone();
        System.out.println("Car is: " + clonedBasiCarNum1.modelName +
                " and it's price is Rs." + clonedBasiCarNum1.price);
        System.out.println();

        // Creates new Ford car from the existong one
        BasicCar clonedBasiCarNum2 = ford_basic.clone();
        clonedBasiCarNum2.price = 200000;
        System.out.println("Car is: " + clonedBasiCarNum2.modelName +
                " and it's price is Rs." + (clonedBasiCarNum2.price +
        BasicCar.setPrice()));
    }
}
