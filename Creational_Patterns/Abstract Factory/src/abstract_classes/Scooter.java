package abstract_classes;

/**
 * Created by ramon on 7/11/16.
 */
public abstract class Scooter {

    protected String modelo;
    protected String color;
    protected int potencia;

    //Constructor
    public Scooter(String modelo, String color, int potencia) {

        this.modelo = modelo;
        this.color = color;
        this.potencia = potencia;
    }

    //Methods
    public abstract void mostrarCaracteristicas();
}
