package abstract_classes;

/**
 * Created by ramon on 7/11/16.
 */
public abstract class Automovil {

    protected String modelo;
    protected String color;
    protected int potencia;
    protected double espacio;

    //Constructor
    public Automovil(String modelo, String color, int potencia, double espacio) {

        this.modelo = modelo;
        this.color = color;
        this.potencia = potencia;
        this.espacio = espacio;
    }

    public abstract void mostrarCaracteristicas();
}
