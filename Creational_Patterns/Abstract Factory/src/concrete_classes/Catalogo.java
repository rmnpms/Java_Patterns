package concrete_classes;

import abstract_classes.Automovil;
import abstract_classes.Scooter;
import interfaces.FabricaVehiculo;

import java.util.Scanner;

/**
 * Created by ramon on 7/11/16.
 */
public class Catalogo {

    public static int nAutos = 3;
    public static int nScooters = 2;

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        FabricaVehiculo fabrica;
        Automovil[] autos = new Automovil[nAutos];
        Scooter[] scooters = new Scooter[nScooters];
        System.out.println("Desea utilizar vehículos eléctricos (1) o a gasolina (2): ");
        String eleccion = reader.next();

        if (eleccion.equals("1")) {
            fabrica = new FabricaVehiculoElectricidad();
        } else {
            fabrica = new FabricaVehiculoGasolina();
        }

        for (int i = 0; i < nAutos; i++) {
            autos[i] = fabrica.creaAutomovil("estandar", "amarillo", 6 + i, 3.2);
        }

        for (int i = 0; i < nScooters; i++) {
            scooters[i] = fabrica.creaScooter("clásico", "rojo", 2 + i);
        }

        for (Automovil automovil : autos) {
            automovil.mostrarCaracteristicas();
        }

        for (Scooter scooter :scooters) {
            scooter.mostrarCaracteristicas();
        }

    }
}
