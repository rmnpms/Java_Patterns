package concrete_classes;

import abstract_classes.Automovil;

/**
 * Created by ramon on 7/11/16.
 */
public class AutomovilGasolina extends Automovil {

    //Constructor
    public AutomovilGasolina(String modelo, String color, int potencia, double espacio) {
        super(modelo, color, potencia, espacio);
    }

    //Methods
    public void mostrarCaracteristicas() {
        System.out.println(
                "tipo: " + this.getClass().getName() + "\n" +
                        "modelo: " + this.modelo + "\n" +
                        "color: " + this.color + "\n" +
                        "potencia: " + this.potencia + "\n" +
                        "espacio: " + this.espacio );
    }
}
