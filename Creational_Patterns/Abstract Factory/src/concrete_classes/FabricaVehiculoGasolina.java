package concrete_classes;

import abstract_classes.Automovil;
import abstract_classes.Scooter;
import concrete_classes.AutomovilGasolina;
import concrete_classes.ScooterGasolina;
import interfaces.FabricaVehiculo;

/**
 * Created by ramon on 7/11/16.
 */
public class FabricaVehiculoGasolina implements FabricaVehiculo {

    public Automovil creaAutomovil(String modelo, String color, int potencia, double espacio) {
        return new AutomovilGasolina(modelo, color, potencia, espacio);
    }

    public Scooter creaScooter(String modelo, String color, int potencia) {
        return new ScooterGasolina(modelo, color, potencia);
    }
}
