package concrete_classes;

import abstract_classes.Automovil;
import abstract_classes.Scooter;
import interfaces.FabricaVehiculo;

/**
 * Created by ramon on 7/11/16.
 */
public class FabricaVehiculoElectricidad implements FabricaVehiculo {

    public Automovil creaAutomovil(String modelo, String color, int potencia, double espacio) {
        return new AutomovilElectricidad(modelo, color, potencia, espacio);
    }

    public Scooter creaScooter(String modelo, String color, int potencia) {
        return new ScooterElectricidad(modelo, color, potencia);
    }
}
