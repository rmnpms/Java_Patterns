package concrete_classes;

import abstract_classes.Scooter;

/**
 * Created by ramon on 7/11/16.
 */
public class ScooterElectricidad extends Scooter {

    //Constructor
    public ScooterElectricidad(String modelo, String color, int potencia) {
        super(modelo, color, potencia);
    }

    //Methods
    public void mostrarCaracteristicas() {
        System.out.println(
                "tipo: " + this.getClass().getName() + "\n" +
                        "modelo: " + this.modelo + "\n" +
                        "color: " + this.color + "\n" +
                        "potencia: " + this.potencia);
    }
}
