package interfaces;

import abstract_classes.Automovil;
import abstract_classes.Scooter;

/**
 * Created by ramon on 7/11/16.
 */
public interface FabricaVehiculo {

    Automovil creaAutomovil(String modelo, String color, int potencia, double espacio);

    Scooter creaScooter(String modelo, String color, int potencia);
}
